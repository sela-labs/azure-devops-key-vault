#  **Create new Key-valut inside your account**

**1.** Enter **&quot;Key vault&quot;** in the search field and press enter. Select Key Vaults under services.

**2.** Select **Add** or the **Create key vault** button to create a new key vault.

**3.** Provide a **name, subscription, resource group and** location for the vault.

**4.** Select **Access Policy** and then select **+Add**** Access Policy** in order to setup a new policy.

**5.** Under **Key permissions** and **Secret permission** select – **``(Get.List)``**

<img alt="image" src="images/image-01.png"  width="50%" height="50%">

**6.** Select the **Select principal** and search for the security principal for Azure Devops, Steps to find Azure Devops principal –

Go to **Azure Devops** and click on **Project Settings => Service Connections**

<img alt="image" src="images/image-02.png"  width="20%" height="20%">

1. Select your **service connection** andclick on **Manage Service Principal**
2. Copy **Display name** to **Select principal** in Task 6.

**7.** Click on Add and **Create**

Go to the key vault you created earlier and create the following secrets-

 **1.**

        Name: VM_Username

        Value: %VM_USERNAME%

**2.**

        Name: VM-Password

        Value: %VM_Password%


**This password should be a strong password!**

Use this site to generate your password:

[https://passwordsgenerator.net/](https://passwordsgenerator.net/)

**Password Length should be at least 12.**

# **Get password from Key Vault and create vm from cli**

After creating  key-vault and give permissions to Azure DevOps to get these secrets, Let&#39;s use Azure DevOps to create new VM under a given subscription and resource group.

The CLI command will use the credentials you implemented above.

1. Go to Azure DevOps and create new pipeline ( use the UI editor)
2. For Select a template use **Empty job**.
3. Under **tasks** search **Azure key vault** and click Add –

<img alt="image" src="images/image-03.png"  width="50%" height="50%">

1. Select your **Azure Subscription** and the **key vault** you created above. In the **Secrets filter** field, you can specify an (\*) to read all secrets.

Now, Lets add the **Azure Cli** that runs the command who create the new VM.

Under **tasks** search **Azure Cli** and click Add-

<img alt="image" src="images/image-04.png"  width="50%" height="50%">

Select your **Azure Subscription** for **Script Location** choose Script inline and paste the following script :

```shell
az vm create  --resource-group %RESOURCE_GROUP_NAME%  --name %VM_NAME%  --image win2016datacenter --admin-username $(VM-Username) --admin-password $(VM-Password)
```

**Update ```%VM_NAME%``` and ```%RESOURCE_GROUP_NAME%``` with your data**

**Save and Queue the pipeline** and wait until the deployment completed.

## *verify the deployment completed successfully:*

**1.** Go to your Resource group and click on the VM you created.

**2.** Click on **Connect** and select **RDP**

**3.** Download the RDP File.

**4.** Open the downloaded file and enter :
```shell
User name – The VM-Username you entered to key-value

Password – The VM-Password you entered to key-value
```

**5.** Congratulations! You now logged in to the VM you created using Azure DevOps using the credentials you supplied to Key-Vault.